// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production    : true,
  apiEndPoint   : 'http://api.caja.local.com/caja/api',
  apiKeyCaptcha : '6LeSImIUAAAAALK_gdGEKHGIDtIQd_07Hn7tOk42',
  baseUrl       : 'https://www.claroshop.com/',
  cookieName    : 'token_cart'
};
