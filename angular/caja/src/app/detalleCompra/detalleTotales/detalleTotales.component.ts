import {Component, OnInit } from '@angular/core';
import {ResumenCompra} from "../../entities/resumenCompra";
import {TuCompraService} from "../../services/tuCompra.service";

@Component({
  selector: 'app-totales',
  templateUrl: './detalleTotales.component.html',
  styleUrls: ['./detalleTotales.component.sass']
})
export class DetalleTotalesComponent implements OnInit{
  public detalleCompra: ResumenCompra;
  constructor(public  tuCompraService: TuCompraService) { }

  ngOnInit(){
    // Obtengo los datos del Resumen Compra para mostrar los totales
    this.tuCompraService.rcActual.subscribe((resumenCompra: ResumenCompra) => {
      this.detalleCompra = resumenCompra;
    })
  }



}
