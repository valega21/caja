import {Component, Input, OnInit} from '@angular/core';
import { TuCompraService } from "../services/tuCompra.service";
import { HttpClient } from '@angular/common/http';
import { ResumenCompra } from '../entities/resumenCompra';
import { GlobalService } from '../services/global.service';

@Component({
    selector:"app-detalleCompra",
    templateUrl: "./detalleCompra.component.html",
    styleUrls: ['./detalleCompra.component.sass']

})
export class detalleCompraComponent implements OnInit {
  public productosKeys;
  @Input() id_forma : string = '';
  @Input() bim      : string = '';
  public detalleCompra: ResumenCompra;
  public loading = false;

  // Bandera para cargar el modulo de Tu Compra cuando la promesa se resuelva
  public infoCargada = false;

  constructor(
    public tuCompraService: TuCompraService, 
    public client: HttpClient,
    public globalService : GlobalService
  ) { }

  ngOnInit() {
    
    this.resumenCompra(this.id_forma, this.bim).then((resumenCompra) => {
      this.detalleCompra = resumenCompra;
      this.productosKeys = Object.keys(this.detalleCompra.productos);
      this.infoCargada   = true;
      
      // Cada vez que carga este componente, setea la nueva versión para que los subscriptores la reciban
      this.tuCompraService.setResumenCompra(this.detalleCompra);
    });
  }

  public resumenCompra(id_forma, bim){
    return this.tuCompraService.resumenCompra(id_forma, bim).toPromise();
  }

  /**
   * 
   */
  public updateData(): void{
    this.resumenCompra(this.id_forma, this.bim).then((resumenCompra) => {
      this.detalleCompra = resumenCompra;
      this.productosKeys = Object.keys(this.detalleCompra.productos);
      this.infoCargada   = true;
      this.tuCompraService.setResumenCompra(this.detalleCompra);
    });
  }

}
