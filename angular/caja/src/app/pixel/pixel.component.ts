import { Component, Input, OnInit } from '@angular/core';
import { DatalayerService } from "../services/datalayer.service";
import { ConfigService } from "../services/config.service";
import { DomSanitizer } from "@angular/platform-browser";
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pixel',
  templateUrl: './pixel.component.html',
  styleUrls: ['./pixel.component.sass']
})
export class PixelComponent implements OnInit {

  @Input('pixelData') pixelData;
  @Input('google') google;

  public srcframe;
  public tagManagerId;
  public initializedSubscription : Subscription;

  constructor(
    private datalayerService  : DatalayerService,
    private configService     : ConfigService,
    private sanitizer         : DomSanitizer,
    private globalService     : GlobalService
  ) { }

  ngOnInit() {
    this.initializedSubscription = this.globalService.initialized.subscribe( initialized => {
      if(initialized == true){
        this.tagManagerId = this.configService.getConfig().tagManagerId;

        this.datalayerService.setPurchase(
          this.pixelData.totalVenta,
          this.pixelData.correoCliente,
          this.pixelData.numPedido,
          this.pixelData.productosDatalayer,
          this.pixelData.productosPixel,
          this.pixelData.productosAdwords,
          this.pixelData.userId
        );

        this.srcframe = this.sanitizer.bypassSecurityTrustResourceUrl(
          "//us.creativecdn.com/tags?id=pr_zI0i2Lh64jxjrfzmL9r6_orderstatus2_"
          + this.pixelData.totalVenta
          + "_"
          + this.pixelData.numPedido
          + "_"
          + this.pixelData.productosDatalayer
          + "&amp;cd=default");

        this.datalayerService.convertionAdwords(this.google);
      }
    });

  }

}
