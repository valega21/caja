import {Component, Input, AfterViewInit, OnInit} from '@angular/core';
import {DatalayerService} from "../../services/datalayer.service";
import {GlobalService} from "../../services/global.service";
import {Subscription} from "rxjs/Rx";

@Component({
  selector: 'app-facebook-pixel',
  templateUrl: './facebook-pixel.component.html',
  styleUrls: ['./facebook-pixel.component.sass']
})
export class FacebookPixelComponent implements OnInit {

  @Input('fbPixel') fbPixel;

  public initializedSubscription: Subscription;

  constructor(private datalayerService: DatalayerService, private globalService: GlobalService) {
  }

  ngOnInit() {
    this.initializedSubscription = this.globalService.initialized.subscribe(initialized => {
      if (initialized == true) {
        if (this.fbPixel.totalVenta !== undefined) {
          let ids = this.fbPixel.productos;
          let ids2 = [];
          let nombre = "";
          let categoria = "";
          for (let i = 0; i < ids.length; i++) {
            ids2.push(ids[i].id);
            nombre = ids[i].name;
            categoria = (ids[i].category !== "null") ? ids[i].category : "ClaroShop";
          }
          if (ids.length < 2) {
            this.datalayerService.setFbPurchase(
              nombre,
              categoria,
              ids[0].id,
              this.fbPixel.totalVenta,
              this.fbPixel.currency,
              this.fbPixel.pedidoNormalizado,
              this.fbPixel.stage,
              this.fbPixel.origen);
          } else {
            this.datalayerService.setFbPurchases(
              ids2,
              this.fbPixel.totalVenta,
              this.fbPixel.currency,
              this.fbPixel.pedidoNormalizado,
              this.fbPixel.stage,
              this.fbPixel.origen);
          }
        }
      }
    });
  }
}
