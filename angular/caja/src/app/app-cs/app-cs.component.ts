import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router'
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';
import { GlobalService } from '../services/global.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-app-cs',
  templateUrl: './app-cs.component.html',
  styleUrls: ['./app-cs.component.sass']
})
export class AppCsComponent implements OnInit {

  private config : Config = new Config();

  /**
   * 
   * @param route 
   * @param router 
   * @param configService 
   * @param cookieService 
   * @param globalService 
   */
  constructor(
    private route : ActivatedRoute,
    private router : Router,
    private configService : ConfigService,
    private cookieService : CookieService,
    private globalService : GlobalService,
  ) {
    this.config = configService.config
    let token = route.snapshot.params.token != undefined ? route.snapshot.params.token : undefined;
    if(token != undefined){
      this.globalService.setIsAppCS(true);
      let date = new Date();
      let expire : number = date.getTime() + 3540
      cookieService.set(this.config.cookieName, token, expire, '/');
      router.navigate(['/direcciones']);
    }
    router.events.subscribe(event => {
      if(event instanceof NavigationStart) {
        globalService.setLoading(true);
      }else if(event instanceof NavigationEnd) {
        globalService.setLoading(false);
      }
    });
  }

  ngOnInit() {}

}
