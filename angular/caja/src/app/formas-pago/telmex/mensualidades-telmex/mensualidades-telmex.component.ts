import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mensualidad } from '../../../entities/mensualidad';
import { MensualidadesTelmexService } from '../../../services/mensualidades-telmex.service';
import { TelmexResponse }  from '../../../entities/telmexResponse';
import { GlobalService } from '../../../services/global.service';
import {DatalayerService} from "../../../services/datalayer.service";

@Component({
    selector:"app-sinCuenta",
    templateUrl: "./mensualidades-telmex.component.html",
    styleUrls: ['./mensualidades-telmex.component.sass']

})

export class mensualidadesTelmexComponent implements OnInit {
  public ruta          : string = 'pagosActiva';
  public title         : string = "2. Selecciona las mensualidades";
  public loading       : boolean = true;
  public breadcrumb    : boolean = true;
  public mensualidades : Mensualidad[];
  public mensualidad   : number = 1;
  public error         : string ;
  public hide          : boolean = true;

  constructor(
    private router: Router,
    private mensualidadesTelmexService: MensualidadesTelmexService,
    private globalService : GlobalService,
    private dataLayerService: DatalayerService
  ){
  }

  ngOnInit() {
    this.getMensualidades()
  }

  /**
   * Obtiene las mensualidades disponibles para el pedido
   */
  private getMensualidades():void {

    this.mensualidadesTelmexService.getMensualidades()
      .subscribe(mensualidades => {
          this.mensualidades = mensualidades

          this.setMensualidadPredet()
          this.setWorkFlow()
      });
  }

  /**
   * Evalua el numero de mensualidades disponibles
   */
  private setWorkFlow():void {

    if( this.mensualidades.length <= 0 ){
      this.goError()
    }//end if

  }

  /**
   * Guardar el numero de mensualidades
   */
  public save(): void{
    if(this.mensualidad){

      this.mensualidadesTelmexService.addMensualidad(this.mensualidad)
        .subscribe( (response : TelmexResponse) => {
          this.revisionMensualidad(response)
        });
    }else{
      this.error = 'No se encuentra definida la mensualidad'
    }//end if
  }

  /**
   *
   * @param response
   */
  private revisionMensualidad(response: TelmexResponse): void{
    if(response.status == true){
        this.goConfirmacion()

    }else{
      this.error = response.error

    }
  }

  /**
   * regresa a la locacion anterior
   */
  private goConfirmacion(): void {

    this.router.navigate(['/confirmacion']);
  }

  /**
   * envia mensaje error
   */
  private goError(): void {

    this.router.navigate(['/error']);
  }

  /**
   * Define el id de la mensualidad seleccionada
   * @param mensualidad
   */
  public setMensualidad(mensualidad: number) : void {
    this.mensualidad = mensualidad
    this.dataLayerService.setCheckoutStep4()

  }
  /**
   * Define la direccion predeterminada
   */
  private setMensualidadPredet():void{
    for( let mensualidad of this.mensualidades ){
      this.mensualidad = mensualidad.mensualidad
      break;
    }
  }
}
