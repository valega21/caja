import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MultipedidoService } from '../../services/multipedido.service';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-payu',
  templateUrl: './payu.component.html',
  styleUrls: ['./payu.component.sass']
})
export class PayuComponent implements OnInit {

  public title    : string ='Selecciona tu Tienda';
  public loading  : boolean = false;
  public tienda   : string;
  public default  : number = 1;
  public tiendas: any = [
      {id: 1, img: './assets/logo-oxxo1.svg',  name: 'Oxxo', note: '*Tu pago con OXXO se autoriza de manera inmediata'},
      {id: 2, img: './assets/logo-seven1.svg', name: 'Seven eleven', note: ''}
  ];
  public ruta = 'pagosActiva';
  constructor(
    private router : Router,
    private multipedidoService : MultipedidoService,
    private globalService : GlobalService
  ) { }

  ngOnInit() {
  }

  goConfirm(tienda : string | number): void {
    
    this.multipedidoService.setDataFormaPago(tienda)
        .subscribe(() => {
            this.router.navigate(['/confirmacion']);
            
        });
  }
}
