import {Component, Input, OnInit} from '@angular/core';
import {MultipedidoService} from "../../services/multipedido.service";
import {Router} from "@angular/router";
import {PaymentMonederoService} from "../../services/payment-monedero.service";
import {Multipedido} from "../../entities/multipedido";
import {Monedero} from "../../entities/monedero";
import {ErrorService} from "../../services/error.service";

@Component({
  selector: 'app-monedero',
  templateUrl: './monedero.component.html',
  styleUrls: ['./monedero.component.sass']
})
export class MonederoComponent implements OnInit {
  public title    : string ='Monedero Claro shop';
  public loading  : boolean = false;
  public ruta = 'pagosActiva';
  public saldo = 0;
  public total = 0;
  public monto = 0;
  public monedero : any;
  public visible = true;
  public error = 'No tienes saldo suficiente. Elige otra forma de pago'
  public hide = true;
  public infoCargada = false;

  constructor(
    private router : Router,
    private multipedidoService : MultipedidoService,
    private monederoService : PaymentMonederoService,
    private errorService : ErrorService
  ) { }

  ngOnInit() {
    this.monederoService.getMonedero()
      .subscribe(monedero => {
        this.monedero = monedero;
        this.saldo = this.monedero.saldo;
        this.monto = this.monedero.monto;
        this.total = this.monedero.total;
        this.infoCargada = true;
        if(this.monedero.status && this.monedero.procesa) {
        } else if(this.monedero.status && !this.monedero.procesa){
          this.visible = false;
          this.hide = true;
        } else {
          this.errorService.setError('No tienes vinculado un Monedero Claro shop en tu cuenta.');
          this.router.navigate(['/formas-pago']);
        }
      });
  }

  public goConfirm(response : any): void {
    this.multipedidoService.setDataFormaPago(response)
      .subscribe(() => {
        this.router.navigate(['/confirmacion']);
      });
  }

  public goFormasPago():void{
    this.router.navigate(['/formas-pago']);
  }

}
