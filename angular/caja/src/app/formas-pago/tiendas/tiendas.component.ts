import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MultipedidoService } from '../../services/multipedido.service';
import { GlobalService } from '../../services/global.service';

@Component({
    selector: "app-tiendas",
    templateUrl: "./tiendas.component.html",
    styleUrls: [ "./tiendas.component.sass" ]
})

export class tiendasComponent implements OnInit{

    public title   : string ='Selecciona tu Tienda';
    public loading : boolean = false;
    public ruta    : string = 'pagosActiva';
    public default : number = 11;
    public tiendas : any = [
        {t: 11, img: "./assets/logo-sanborns1.svg", idFormaPago: "4450"},
        {t: 12, img: "./assets/logo-sears1.svg", idFormaPago: "4450"}
    ]
    /**
     * Constructor
     * @param router 
     * @param multipedidoService 
     * @param globalService 
     */
    constructor(
        public router: Router,
        private multipedidoService: MultipedidoService,
        private globalService : GlobalService
    ){}

    ngOnInit(){
        window.scrollTo(0,0);
    }

    /**
     * Define la forma de pago
     * @param tienda 
     */
    public goConfirm(tienda : string | number): void {
        
        this.multipedidoService.setDataFormaPago(tienda)
            .subscribe(() => {
                this.router.navigate(['/confirmacion']);
                
            });
    }
}
