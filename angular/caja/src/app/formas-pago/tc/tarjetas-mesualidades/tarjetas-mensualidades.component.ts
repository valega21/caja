import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mensualidad } from '../../../entities/mensualidad';
import { MensualidadesService } from '../../../services/mensualidades.service';
import { GlobalService } from '../../../services/global.service';
import {MultipedidoService} from "../../../services/multipedido.service";
import {Multipedido} from "../../../entities/multipedido";
import {ConfigService} from "../../../services/config.service";

@Component({
  selector:"app-tarjetasMeses",
  templateUrl: "./tarjetas-mensualidades.component.html"

})

export class tarjetasMesesComponent implements OnInit{

  public isLoad        : boolean = false;
  public title         : string  = "Selecciona las Mensualidades";
  public loading       : boolean = true;
  public breadcrumb    : boolean = true;
  public mensualidades : Mensualidad[];
  public mensualidad   : number = 1;
  public montoPago     : number;
  public ruta          : string = 'pagosActiva';
  public idFormaPago   : number;
  public idFpT1pagos   : number;
  public idFpT1Amex    : number;

  /**
   *
   * @param {Router} router
   * @param {MensualidadesService} mensualidadesService
   * @param {GlobalService} globalService
   * @param {MultipedidoService} multipedidoService
   * @param {ConfigService} configService
   */
  constructor(
    private router: Router,
    private mensualidadesService: MensualidadesService,
    private globalService : GlobalService,
    private multipedidoService : MultipedidoService,
    private configService : ConfigService)
    {
      let config = this.configService.getConfig();
      this.idFpT1pagos = config.t1pagosId;
      this.idFpT1Amex  = config.t1pagosAmex;
    }

  ngOnInit() {
    this.multipedidoService.getMultipedido().subscribe(multipedido => {
      console.log(multipedido);
      this.idFormaPago = multipedido.idFormaDePago;
      this.getMensualidades()
      console.log(multipedido)
    })
  }

  /**
   * Obtiene las mensualidades disponibles para el pedido
   */
  private getMensualidades():void {

    this.mensualidadesService.getMensualidades()
      .subscribe(mensualidades => {
          this.mensualidades = mensualidades
          this.setMensualidadPredet()
          this.isLoad = true
          this.setWorkFlow()
      });
  }

  /**
   * Evalua el numero de mensualidades disponibles
   */
  private setWorkFlow():void {

    if( this.mensualidades.length <= 0 ){
        this.goFacturacion();
    }//end if

  }

  /**
   * Guardar el numero de mensualidades
   */
  save(): void{

    this.mensualidadesService.addMensualidad(this.mensualidad, this.montoPago)
      .subscribe(() => {
          this.goFacturacion();
      });

  }
  /**
   * regresa a la facturación
   */
  goFacturacion(): void {
    console.log('en mensualidades, forma pago: ' + this.idFormaPago)
    if(this.idFormaPago === this.idFpT1pagos || this.idFormaPago === this.idFpT1Amex){
      this.router.navigate(['/confirmacion']);
    } else {
      this.router.navigate(['/facturacion']);
    }
  }


  /**
   * Define la mensualidad seleccionada
   * @param {number} mensualidad
   * @param {number} pago
   */
  setMensualidad(mensualidad: number, pago: number) : void {
    this.mensualidad = mensualidad;
    this.montoPago = pago;
  }

  /**
   * Define la mensualidad predeterminada
   */
  private setMensualidadPredet():void{
    for( let mensualidad of this.mensualidades ){
      this.mensualidad = mensualidad.mensualidad
      this.montoPago = mensualidad.pago;
      mensualidad.active = true
      break;
    }
  }

}
