import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tarjeta } from '../../entities/tarjeta';
import { TarjetaForm } from '../../entities/tarjetaForm';
import { TarjetaResponse } from '../../entities/tarjetaResponse';
import { TarjetasCreditoDebitoService } from '../../services/tarjetas-credito-debito.service';
import { CardValidatorService } from '../../services/card-validator.service';
import { isNumber } from 'util';
import { ErrorService } from "../../services/error.service";
import { Expiry } from '../../entities/expiry';
import { GlobalService } from '../../services/global.service';
import { Subscription } from 'rxjs';
import {DatalayerService} from "../../services/datalayer.service";

@Component({
  selector:"app-tc",
  templateUrl: "./tc.component.html",
  styleUrls: ['./tc.component.sass']
})

export class tcComponent implements OnInit{

  @Input() tarjeta : TarjetaForm = new TarjetaForm();
  @Input() expiry  : Expiry = new Expiry();

  public title      : string = "Agregar Nueva Forma de Pago";
  public loading    : boolean = true;
  public breadcrumb : boolean = true;
  public disabled   : boolean = false;
  public id         : number = 0;
  public show       : boolean = false ;
  public cvv        : string;
  public anios      : any = []
  public error      : any;
  public ruta       : string = 'pagosActiva';
  public cvvHelp    : string = 'El cvv se encuentra en la parte posterior de la tarjeta'
  public isDepartamental : boolean = false;
  public isEdit     : boolean = false;
  public cvvSubscription : Subscription;
  public hide       : boolean = true;
  public rules = {
    valid       : true,
    nombre      : { error: false },
    numero      : { error: false },
    vencimiento : { error: false },
    codigo      : { error: false }
  }
  public meses : any = [

    { mes: "01", name: "01 - Enero" },
    { mes: "02", name: "02 - Febrero" },
    { mes: "03", name: "03 - Marzo" },
    { mes: "04", name: "04 - Abril" },
    { mes: "05", name: "05 - Mayo" },
    { mes: "06", name: "06 - Junio" },
    { mes: "07", name: "07 - Julio" },
    { mes: "08", name: "08 - Agosto" },
    { mes: "09", name: "09 - Septiembre" },
    { mes: "10", name: "10 - Octubre" },
    { mes: "11", name: "11 - Noviembre" },
    { mes: "12", name: "12 - Diciembre" }

]

  /**
   *
   * @param {Router} router
   * @param {ActivatedRoute} route
   * @param {TarjetasCreditoDebitoService} tarjetasService
   * @param {CardValidatorService} cardValidatorService
   * @param {ErrorService} errorService
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tarjetasService: TarjetasCreditoDebitoService,
    private cardValidatorService: CardValidatorService,
    private errorService : ErrorService,
    private globalService : GlobalService,
    private datalayerService : DatalayerService
  ){
  }

  ngOnInit() {
    if(this.route.snapshot.url[1].path === 'departamentales'){
      this.isDepartamental = true;
    }
    this.getYears()
    this.getTarjeta()
    this.cvvSubscription = this.globalService.cvv
      .subscribe( cvv => {
        this.cvv = cvv
      });
    this.errorService.errorActual.subscribe( error => {
      this.error = error
    });
  }

  ngOnDestroy(){
    this.errorService.setError('')
  }
  /**
   * Actualiza el valor de la variable expiry
   */
  public updateExpiry(){
    let anio  = this.expiry.anio == undefined ? '••' : this.expiry.anio
    let mes   = this.expiry.mes == undefined ? '••' : this.expiry.mes
    this.expiry.expiry       = mes + '/' + anio
    this.tarjeta.vencimiento = this.expiry.expiry
    if( (anio != undefined && anio != '••') && (mes != undefined && mes != '••') ){
      this.validateForm('vencimiento')
    }//end if
    setTimeout(function(){
      let event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true)
      let tarjeta: HTMLElement = document.querySelector('input[id="expiry"]') as HTMLElement
      tarjeta.dispatchEvent(event);
    }, 50)
  }

  /**
   * Obtiene el objeto tarjeta por medio del id
   */
  getTarjeta(): void{
    let id = +this.route.snapshot.paramMap.get('id');
    this.id = id;
    if( isNumber(id) && id > 0){
      this.isEdit = true
      this.tarjetasService.getTarjeta(id)
      .subscribe((tarjeta: Tarjeta) => {
        this.disabled = true
        this.setObjectTarjetaForm(tarjeta)
        this.initCardFormat()

      });
    }else{

    }//end if
  }

  /**
   * regresa a la locacion anterior
   */
  goListTarjetas(): void {

    this.router.navigate(['/formas-pago/guardadas']);
  }

  /**
   * Guardar el objeto tarjeta
   */
  save(): void{

    let validate = this.validateForm(undefined);
    if(validate){
      this.tarjeta.numero = this.tarjeta.numero.replace(/[^0-9*]/g, '')
      this.globalService.setCvv(this.tarjeta.codigo)
      this.tarjetasService.addTarjeta(this.tarjeta)
        .subscribe((response: TarjetaResponse) => {
          this.setPago(response.id)
        });
    }else{

      if(this.isDepartamental){
        this.error = 'Por favor verifica tu tarjeta departamental';
      }
    }//end if
  }

  /**
   * Establese una tarjeta como forma de pago
   * @param id
   */
  setPago(id: number): void{

    this.tarjetasService.setTarjetaPago(id, 3912)
      .subscribe((resultado) =>{

        if(resultado.idFPago == 4384 /* sears */  ||
          resultado.idFPago == 4379 /* sanborns */          ||
          resultado.idFPago == 4375 /* sears */             ||
          resultado.idFPago == 4385 /* sears */) {
          this.router.navigate(['/formas-pago/departamental/promociones']);
        } else {
          this.router.navigate(['/formas-pago/tarjeta/mensualidades']);
        }
      })
  }

  /**
   * Define los parametros requeridos para el formulario tarjeta
   * @param tarjeta
   */
  setObjectTarjetaForm(tarjeta: Tarjeta) : void {
    this.tarjeta.id          = tarjeta.id
    this.tarjeta.nombre      = tarjeta.nombre + ' ' + tarjeta.apellido
    this.tarjeta.numero      = this.formatCard(tarjeta.bm, tarjeta.tipo)
    this.tarjeta.vencimiento = tarjeta.mes +'/'+ tarjeta.anio
    this.tarjeta.codigo      = this.formatCode(tarjeta.tipo)
    this.tarjeta.tipo        = tarjeta.tipo
    this.expiry.mes          = tarjeta.mes
    this.expiry.anio         = tarjeta.anio
    this.expiry.expiry       = tarjeta.mes +'/'+ tarjeta.anio
    if(tarjeta.tipo == 'sears' || tarjeta.tipo == 'sanborns'){
      this.expiry.expiry       = ''
      this.isDepartamental     = true
    }

  }

  /**
   * @param digitos
   * @param type
   */
  public formatCard(digitos:string, type:string):string {

    let response : string = ''
    let gaps     : number[] = [4, 8, 12];
    let card     : string = '';

    switch (type) {
      case 'amex':
        card = digitos + '*********'
        gaps = [4, 10]
        break;
      case 'mastercard':
      case 'visa':
        card = digitos + '**********';
        gaps = [4, 8, 12]
        break;
      case 'sanborns':
      case 'sears':
        card = digitos + '******';
        gaps = [2, 8]
        break;
      default:
        card = digitos + '**********';
        gaps = [4, 8, 12]
        break;
    }
    for(var i = 0; i < card.length; i++){
      for(let gap of gaps){
        if(i == gap && i > 0){
          response = response + ' '
        }//end if
      }//end for
      response = response + card[i]
    }//end for

    return response
  }

  /**
   * @param digitos
   * @param type
   */
  public formatCode(type:string):string {
    let response : string;
    switch (type) {
      case 'amex':
        response = '****';
        break;
      default:
        response = '***';
        break;
    }
    return response
  }

  /**
   *
   */
  public initCardFormat():void{
    setTimeout(function() {
      let event = document.createEvent('HTMLEvents');
      event.initEvent('keyup', false, true)
      let number: HTMLElement = document.querySelector('input[name="number"]') as HTMLElement
      let name: HTMLElement = document.querySelector('input[name="name"]') as HTMLElement
      let expiry: HTMLElement = document.querySelector('input[name="expiry"]') as HTMLElement
      let code : HTMLElement = document.querySelector('input[name="cvc"]') as HTMLElement
      number.dispatchEvent(event)
      name.dispatchEvent(event)
      expiry.dispatchEvent(event)
    }, 500);
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean {
    let response = this.cardValidatorService.validateCardForm(this.tarjeta);
    this.cvvHelp = response.tipo == 'amex' ? 'En caso de ser amex se muestra en la parte frontal de la tarjeta' : 'El cvv se encuentra en la parte posterior de la tarjeta';
    this.tarjeta.tipo = response.tipo;
    if( field !== undefined ){
      this.rules[field].error = response[field].error
    }else{
      this.rules = response;
    }
    return response.valid;
  }

  /**
   * Get years
   */
  private getYears():any{
    let year = new Date().getFullYear()
    for(var entry = year; entry < year + 8; entry++){
      let text = entry.toString()
      let anio = text.substring(text.length -2)
      this.anios.push({anio: anio, name: text})
    }//end for
  }

  public setStep4($event){
    this.datalayerService.setCheckoutStep4();
  }

}
