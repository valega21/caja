import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { isNumber } from 'util';

import { Multipedido } from '../entities/multipedido';
import { MultipedidoService } from '../services/multipedido.service';
import { Tarjeta } from '../entities/tarjeta';
import { TarjetasCreditoDebitoService } from '../services/tarjetas-credito-debito.service';
import { Direccion } from '../entities/direccion';
import { DireccionesService } from '../services/direcciones.service';
import { Zonificacion } from '../entities/zonificacion';
import { ZonificacionService } from '../services/zonificacion.service';
import { Validator } from '../entities/validator';
import { ValidatorService } from '../services/validator.service';
import { SucursalesService } from '../services/sucursales.service';
import { Sucursal } from '../entities/sucursal';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
})

export class facturacionComponent implements AfterViewInit {

  @Input() direccionForm : Direccion = new Direccion();

  public url         : string = '/formas-pago/guardadas'
  public title       : string = "Dirección de facturación";
  public show        : boolean = false ;
  public hide        : boolean = true;
  public envio       : string = 'Envio';
  public entrega     : string = 'Entrega';
  public loading     : boolean = false;
  public mensualidad : number = 1;
  public multipedido : Multipedido;
  public direccion   : Direccion = new Direccion();
  public tarjeta     : Tarjeta = new Tarjeta();
  public colonias    : String[] = [];
  public showCode    : boolean = false;
  public showEdit    : boolean = false;
  public idFormaPago : number;
  public infoCargada : boolean = false;
  public nombrePromo : string;
  public clickRecoge : boolean = false;
  public sucursal    : Sucursal = new Sucursal();
  public isLoadDireccion : boolean = false;
  public isLoadTarjeta   : boolean = false;
  public isLoadSucursal  : boolean = false;
  public error           : boolean = false;
  public mensaje         : string = '';
  public default         : boolean = true;
  private conditions   : string[] = ['idCliente', 'idFormaDePago', 'idDireccion', 'formaDePago'];
  public rules = {
    'direccion' : new Validator(
      'direccion',
      '',
      'alphanumeric',
      'El campo direccion es requerido'
    ),
    'numeroExterior': new Validator(
      'numeroExterior',
      '',
      'number',
      'El campo numero exterior es requerido'
    ),
    'codigoPostal'  : new Validator(
      'codigoPostal',
      '',
      'postalCode',
      'El campo codigo postal es requerido'
    ),
    'estado'        : new Validator(
      'estado',
      '',
      'formatstring',
      'El campo estado es requerido'
    ),
    'municipio'     : new Validator(
      'municipio',
      '',
      'formatstring',
      'El campo municipio es requerido'
    ),
    'colonia'     : new Validator(
      'colonia',
      '',
      'formatstring',
      'El campo colonia es requerido'
    )
  };
  public ruta = 'pagosActiva';

  /**
   *
   * @param {Router} router
   * @param {MultipedidoService} multipedidoService
   * @param {TarjetasCreditoDebitoService} tarjetasService
   * @param {DireccionesService} direccionesService
   * @param {ZonificacionService} zonificacionService
   * @param {ValidatorService} validatorService
   * @param {SucursalesService} sucursalesService
   * @param {GlobalService} globalService
   */
  constructor(
    private router: Router,
    private multipedidoService: MultipedidoService,
    private tarjetasService: TarjetasCreditoDebitoService,
    private direccionesService: DireccionesService,
    private zonificacionService: ZonificacionService,
    private validatorService: ValidatorService,
    private sucursalesService: SucursalesService,
    private globalService : GlobalService
  ) {
  }

  ngAfterViewInit() {
    this.getMultipedido()
  }

  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido():void {

    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
          this.multipedido = multipedido
          this.validateConditions();
          this.mensualidad = multipedido.mensualidades
          this.clickRecoge = multipedido.clickRecoge
          this.getSucursal(multipedido.sucursal)
          this.getDireccion(multipedido.idDireccion)
          this.getTarjeta(multipedido.formaDePago)
          this.idFormaPago = this.multipedido.idFormaDePago;
          this.nombrePromo = this.multipedido.nombrePromocion;

      });
  }

  /**
   * Obtiene el objeto sucursal por medio del id
   */
  private getSucursal(id: number): void{
    if( isNumber(id) && id > 0){
      this.sucursalesService.getSucursal(id)
        .subscribe(( sucursal: Sucursal ) => {
          this.sucursal = sucursal
          this.isLoadSucursal = true
        });
    }
  }

  /**
   * Obtiene el objeto direccion por medio del id
   */
  private getDireccion(id: number): void{
    if( isNumber(id) && id > 0){
      this.direccionesService.getDireccion(id)
        .subscribe((direccion: Direccion) => {
          this.direccion = direccion;
          this.isLoadDireccion = true
        });
    }
  }

  /**
   * Obtiene el objeto tarjeta por medio del id
   */
  private getTarjeta(id: number): void{
    if( isNumber(id) && id > 0){
      this.tarjetasService.getTarjeta(id)
      .subscribe((tarjeta: Tarjeta) => {
        this.tarjeta = tarjeta
        this.isLoadTarjeta = true
      });
    }
  }

  /**
   * Guardar el objeto direccion
   */
  save(): void{

    if(this.show === true){
      let validate = this.validateForm(undefined);
      if( validate ){
        this.direccionesService.setDireccionFacturacion(this.direccionForm)
          .subscribe(() => this.goConfirm());
      }else{

      }//end if
    }else{

      this.goConfirm()
    }//end if

  }

  /**
   * Obtener zonificacion
   */
  getZoning(cp: string) : void {
    if(cp !== undefined && cp.length >= 5){

      this.zonificacionService.getZonificacion(cp)
        .subscribe(zonificacion => {
          this.updateZoning(zonificacion)

        });
    }else{
      this.rules.codigoPostal.error = true
    }//end if
  }

  /**
   * Actualiza los campos de zonificacion
   * @param zonificacion
   */
  updateZoning(zonificacion: Zonificacion): void{
    this.direccionForm.estado    = zonificacion.estado;
    this.direccionForm.ciudad    = zonificacion.ciudad;
    this.direccionForm.municipio = zonificacion.municipio;
    this.direccionForm.colonia   = undefined
    this.default                 = true
    this.colonias = zonificacion.colonias;
    if(!zonificacion.estatus){
      this.error = true
      this.hide  = true
      this.mensaje = zonificacion.error
    } else {
      this.error = false
      this.mensaje = ''
      this.rules.codigoPostal.error = false
    }
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean{
    let valid: boolean = true;
    let response = this.rules
    this.setRulesValidate(field);
    if( field == undefined){
      response = this.validatorService.validate(this.rules);
    }else{
      let param = {[field] : this.rules[field]}
      response = this.validatorService.validate(param);
    }
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          this.rules[key].error = response[key].error
          valid = false;
        }//end if
      }//end if
    }//end for
    //this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(field : string | undefined ): void{
    if( field !== undefined ){
      this.rules[field].value = this.direccionForm[field]
    }else{
      this.rules.direccion.value      = this.direccionForm.direccion;
      this.rules.codigoPostal.value   = this.direccionForm.codigoPostal;
      this.rules.numeroExterior.value = this.direccionForm.numeroExterior;
      this.rules.estado.value         = this.direccionForm.estado;
      this.rules.municipio.value      = this.direccionForm.municipio;
      this.rules.colonia.value        = this.direccionForm.colonia;
    }//end if
  }

  /**
   * Redirecciona a la url del flujo
   */
  goConfirm(): void {
    this.router.navigate(['/confirmacion']);
  }

  /**
   * validate conditions
   */
  private validateConditions(): void{

    if( this.multipedido.clickRecoge ){
      return;
    }else{
      switch(this.multipedido.idFormaDePago){
        case 5:
          this.conditions = ['idCliente', 'idFormaDePago', 'idDireccion'];
          break;
        default:
          this.conditions = this.conditions
          break;
      }//end switch
    }//end if
    for (let key of this.conditions){
      if ( typeof this.multipedido[key] == 'undefined' || !this.multipedido[key]){
        this.router.navigate(['/direcciones']);
      }//end if
    }//end if
  }

  /**
   * Router back
   */
  public routerBack(){
      this.router.navigate([this.url]);
  }
}
