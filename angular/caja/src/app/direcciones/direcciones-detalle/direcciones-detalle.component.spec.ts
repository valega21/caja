import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DireccionesDetalleComponent } from './direcciones-detalle.component';

describe('DireccionesDetalleComponent', () => {
  let component: DireccionesDetalleComponent;
  let fixture: ComponentFixture<DireccionesDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DireccionesDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DireccionesDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
