import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Direccion } from '../../entities/direccion';
import { Zonificacion } from '../../entities/zonificacion';
import { Validator } from '../../entities/validator';
import { DireccionResponse } from '../../entities/direccionResponse';
import { DireccionesService } from '../../services/direcciones.service';
import { ZonificacionService } from '../../services/zonificacion.service';
import { ValidatorService } from '../../services/validator.service';
import { DatalayerService } from "../../services/datalayer.service";
import { GlobalService } from '../../services/global.service';
import { Subscription } from 'rxjs';
import { ConfigService } from '../../services/config.service';
import { Config } from '../../entities/config';

@Component({
    selector:"app-direccionNueva",
    templateUrl: "./direccion-nueva.component.html",
    styleUrls: ['./direccion-nueva.component.sass']

})
export class direccionNuevaComponent implements OnInit{

  @Input() direccion: Direccion;
  @Input() colonias: String[] = [];

  public url      : string = '/direcciones'
  // public url      : string = '/Caja/direcciones'
  public title    : string = 'Agregar Nueva Dirección';
  public loading  : boolean = false;

  public error    : boolean = false;
  public hide     : boolean = true;
  public mensaje  : string = '';
  public default  : boolean = true;
  public rules = {
    'nombre' : new Validator(
      'nombre',
      '',
      'string',
      'El campo nombre es requerido'
    ),
    'direccion' : new Validator(
      'direccion',
      '',
      'alphanumeric',
      'El campo direccion es requerido'
    ),
    'telefono' : new Validator(
      'telefono',
      '',
      'phone',
      'El campo telefono es requerido'
    ),
    'numeroExterior': new Validator(
      'numeroExterior',
      '',
      'alphanumeric',
      'El campo numero exterior es requerido'
    ),
    'codigoPostal'  : new Validator(
      'codigoPostal',
      '',
      'postalCode',
      'El campo codigo postal es requerido'
    ),
    'estado'        : new Validator(
      'estado',
      '',
      'formatstring',
      'El campo estado es requerido'
    ),
    'municipio'     : new Validator(
      'municipio',
      '',
      'formatstring',
      'El campo municipio es requerido'
    ),
    'colonia'     : new Validator(
      'colonia',
      '',
      'formatstring',
      'El campo colonia es requerido'
    )
  };
  public ruta : string = 'direccionActiva';
  public direccionesSubscription : Subscription;
  public config : Config = new Config();

  /**
   *
   * @param router
   * @param route
   * @param direccionesService
   * @param zonificacionService
   * @param location
   * @param validatorService
   * @param datalayerService
   * @param globalService
   * @param configService
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private direccionesService: DireccionesService,
    private zonificacionService: ZonificacionService,
    private location: Location,
    private validatorService: ValidatorService,
    private datalayerService : DatalayerService,
    private globalService : GlobalService,
    private configService : ConfigService
  ) { 
    this.config = this.configService.config
  }

  ngOnInit() {
    this.direccionesSubscription = this.globalService.direcciones
    .subscribe(direcciones =>{
      if( direcciones == false ){
        this.url = this.config.baseUrl + 'carrito'
      }
    });
    this.getDireccion();
    window.scrollTo(0,0);
  }

  /**
   * Obtiene el objeto direccion por medio del id
   */
  getDireccion(): void{
      this.direccion = new Direccion;
  }
  /**
   * regresa a la locacion anterior
   */
  goDireccionesList(): void {
    this.router.navigate(['/direcciones']);
  }

  /**
   * Guardar el objeto direccion
   */
  save(): void{
    
    let validate = this.validateForm(undefined);
    if(validate){
      this.direccionesService.addDireccion(this.direccion)
        .subscribe( (response : DireccionResponse) => {
          //this.goDireccionesList()
          // DataLayer Direcciones
          this.datalayerService.setCheckoutStep2();          
          this.envio(response.id)
        });
    }else{
      
    }//end if
  }

  /**
   * Establese una direccion como predeterminada
   * @param id
   */
  envio(id: number): void{
    
    this.direccionesService.setDireccionEnvio(id)
      .subscribe(() => {
        this.router.navigate(['/formas-pago/guardadas']);
        
      });
  }

  /**
   * Obtener zonificacion
   */
  getZoning(cp: string) : void {
    if(cp !== undefined && cp.length >= 5){
      
      this.zonificacionService.getZonificacion(cp)
        .subscribe(zonificacion => {
          this.updateZoning(zonificacion)
          
        });
    }else{
      this.rules.codigoPostal.error = true
    }//end if
  }

  /**
   * Actualiza los campos de zonificacion
   * @param zonificacion
   */
  updateZoning(zonificacion: Zonificacion): void{
    this.direccion.estado    = zonificacion.estado;
    this.direccion.ciudad    = (zonificacion.ciudad && zonificacion.ciudad.length>1)? zonificacion.ciudad : 'N/A';
    this.direccion.municipio = zonificacion.municipio;
    this.direccion.colonia   = undefined
    this.default             = true
    this.colonias            = zonificacion.colonias;
    if(!zonificacion.estatus){
      this.error   = true
      this.hide      = true
      this.mensaje = zonificacion.error
    } else {
      this.error   = false
      this.mensaje = ''
      this.rules.codigoPostal.error = false
    }
  }

  /**
   * Genera la validacion del formulario
   * @return boolean
   */
  public validateForm(field : string | undefined ): boolean{
    let valid: boolean = true;
    let response = this.rules
    this.setRulesValidate(field);
    if( field == undefined){
      response = this.validatorService.validate(this.rules);
    }else{
      let param = {[field] : this.rules[field]}
      response = this.validatorService.validate(param);
    }
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          this.rules[key].error = response[key].error
          valid = false;
        }//end if
      }//end if
    }//end for
    //this.rules = response;

    return valid
  }

  /**
   * Define las reglas de validacion para el formulario
   */
  private setRulesValidate(field : string | undefined ): void{
    if( field !== undefined ){
      this.rules[field].value = this.direccion[field]
    }else{
      this.rules.nombre.value         = this.direccion.nombre;
      this.rules.direccion.value      = this.direccion.direccion;
      this.rules.telefono.value       = this.direccion.telefono;
      this.rules.codigoPostal.value   = this.direccion.codigoPostal;
      this.rules.numeroExterior.value = this.direccion.numeroExterior;
      this.rules.estado.value         = this.direccion.estado;
      this.rules.municipio.value      = this.direccion.municipio;
      this.rules.colonia.value        = this.direccion.colonia;
    }//end if
  }

  /**
   * Router back
   */
  public routerBack(){
    // if(this.url == '/Caja/direcciones'){
    if(this.url == '/direcciones'){
      this.router.navigate(['/direcciones']);
    }else{
      window.location.href = this.config.baseUrl + 'carrito'
    }// end if
  }
}
