import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-error-app',
  templateUrl: './error-app.component.html',
  styleUrls: ['./error-app.component.sass']
})

export class errorAppComponent implements OnInit {

  public error   : boolean = false;
  

  /**
   * Global service
   * @param globalService 
   */
  constructor(
    private globalService : GlobalService
  ) { }

  ngOnInit() {
    window.scrollTo(0,0);
    

  }
 
}
