import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaPagoMonederoComponent } from './forma-pago-monedero.component';

describe('FormaPagoMonederoComponent', () => {
  let component: FormaPagoMonederoComponent;
  let fixture: ComponentFixture<FormaPagoMonederoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaPagoMonederoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaPagoMonederoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
