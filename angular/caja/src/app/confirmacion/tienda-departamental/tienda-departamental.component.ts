import { Component, Input, OnInit } from '@angular/core';
import { FormaPago } from '../../entities/formaPago';
import { Multipedido } from '../../entities/multipedido';

@Component({
  selector: 'app-tienda-departamental',
  templateUrl: './tienda-departamental.component.html',
  styleUrls: ['./tienda-departamental.component.sass']
})
export class TiendaDepartamentalComponent implements OnInit {

  @Input ('multipedido') multipedido : Multipedido = new Multipedido();
  public tienda;
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
    if(this.multipedido.formaDePago==11){
      this.tienda = 'SANBORNS';
    } else {
      this.tienda = 'SEARS';
    }


  }

}
