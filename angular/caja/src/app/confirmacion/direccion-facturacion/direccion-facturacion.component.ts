import { Component, Input, OnInit } from '@angular/core';
import { Direccion } from '../../entities/direccion';

@Component({
  selector: 'app-direccion-facturacion',
  templateUrl: './direccion-facturacion.component.html',
  styleUrls: ['./direccion-facturacion.component.sass']
})
export class DireccionFacturacionComponent implements OnInit {

  @Input('facturacion') facturacion : Direccion = new Direccion();

  constructor() { }
  
  ngOnInit() { }
}

