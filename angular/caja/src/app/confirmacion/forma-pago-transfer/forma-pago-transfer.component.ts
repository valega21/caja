import { Component, OnInit, Input } from '@angular/core';
import { Checkout } from '../../entities/checkout';

@Component({
  selector: 'app-forma-pago-transfer',
  templateUrl: './forma-pago-transfer.component.html',
  styleUrls: ['./forma-pago-transfer.component.sass']
})

export class FormaPagoTransferComponent implements OnInit {

  @Input() checkout: Checkout = new Checkout()
  @Input('showEdit') showEdit : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
