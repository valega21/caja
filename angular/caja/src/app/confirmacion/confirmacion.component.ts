import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { isNumber } from 'util';
import { Multipedido } from '../entities/multipedido';
import { MultipedidoService } from '../services/multipedido.service';
import { Tarjeta } from '../entities/tarjeta';
import { TarjetasCreditoDebitoService } from '../services/tarjetas-credito-debito.service';
import { Direccion } from '../entities/direccion';
import { DireccionesService } from '../services/direcciones.service';
import { Checkout } from '../entities/checkout';
import { DeviceFingerPrintService } from '../services/device-finger-print.service';
import { PaymentCreditoDebitoService } from '../services/payment-credito-debito.service';
import { Validator } from '../entities/validator';
import { ValidatorService } from '../services/validator.service';
import { PaymentDepositoService } from "../services/payment-deposito.service";
import { PaymentPagoTiendaService } from '../services/payment-pago-tienda.service';
import { PaymentPaypalService } from '../services/payment-paypal.service';
import { PaymentPayuService } from '../services/payment-payu.service';
import { PaymentTelmexService } from '../services/payment-telmex.service';
import { PaymentTransferService } from '../services/payment-transfer.service';
import { Captcha } from '../entities/captcha';
import { PaymentTarjetaDepartamentalService } from "../services/payment-tarjeta-departamental.service";
import { ErrorService } from "../services/error.service";
import { ConfigService } from "../services/config.service";
import { Config } from '../entities/config';
import { SucursalesService } from '../services/sucursales.service';
import { Sucursal } from '../entities/sucursal';
import { GlobalService } from '../services/global.service';
import { Subscription } from 'rxjs';
import {PaymentMonederoService} from "../services/payment-monedero.service";
import {T1TarjetasService} from "../services/t1-tarjetas.service";
import {T1Response} from "../entities/t1Response";
import {T1Tarjeta} from "../entities/t1Tarjeta";
import {PaymentT1PagosService} from "../services/payment-t1-pagos.service";
import {TarjetaPagosService} from "../services/tarjeta-pagos.service";
import {T1TarjetaUpdate} from "../entities/T1TarjetaUpdate";
import {T1Direccion} from "../entities/T1Direccion";

const fpCreditoDebito : number = 3912;
const fpAmex          : number = 344;
const fpTelmex        : number = 3158;
const fpTarjetasDepartamentales : number = 4384;
const fpCreditoRevolvente       : number = 4375;
const fpMejorPlan               : number = 4385;
const fpTiendasDepartamentales  : number = 4450;
const fpDepositoBancario        : number = 4;
const fpTiendasConveniencia     : number = 1389;
const fpPaypal                  : number = 5;
const fpMonedero                : number = 4466;

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.sass']
})
export class ConfirmacionComponent implements OnInit {

  @Input() checkout: Checkout = new Checkout();
  @ViewChild('detallePago') detallePago ;

  public isLoad          : boolean = false;
  public isLoadDireccion : boolean = false;
  public isLoadTarjeta   : boolean = false;
  public isLoadSucursal  : boolean = false;
  public isSetError      : boolean = false;
  public title         : string = 'Confirmación';
  public loading       : boolean = false;
  public mensualidad   : number = 1;
  public multipedido   : Multipedido = new Multipedido();
  public direccion     : Direccion = new Direccion();
  public tarjeta       : Tarjeta = new Tarjeta();
  public captcha       : Captcha = new Captcha();
  public nombre        : string;
  public fechaEstimada : string = '31-Diciembre-2018';
  public error         : string;
  public formaPago     : number;
  public id_forma      : number;
  public bim           : number;
  public showCode      : boolean = true;
  public showEdit      : boolean = true;
  public paymentTitle  : string;
  public idFormaPago   : number;
  public nombrePromo   : string;
  public clickRecoge   : boolean = false;
  public sucursal      : Sucursal = new Sucursal();
  public conditions    : string[] = ['idCliente', 'idFormaDePago', 'idDireccion', 'formaDePago'];
  public facturacion   : Direccion = new Direccion();
  public hide          : boolean = true;
  public disabled      : boolean = true;
  public codeSubscription : Subscription;
  public config        : Config = new Config();
  public cvvHelp       : string = 'El cvv se encuentra en la parte posterior de la tarjeta'
  public rules = {
    'codigo' : new Validator(
      'codigo',
      'valid',
      'string',
      'El campo codigo es requerido'
    ),
    'captcha' : new Validator(
      'captcha',
      'valid',
      'string',
      'El campo captcha es requerido'
    )
  };
  // Bandera para cargar el modulo de Tu Compra cuando la promesa se resuelva
  public infoCargada = false;
  public ruta = 'confirmacionActiva'
  public montoPago   : number = 0;
  public idFpT1pagos : number;
  public idFpT1Amex  : number;


  /**
   *
   * @param {Router} router
   * @param {ActivatedRoute} route
   * @param {MultipedidoService} multipedidoService
   * @param {TarjetasCreditoDebitoService} tarjetasService
   * @param {DireccionesService} direccionesService
   * @param {DeviceFingerPrintService} deviceService
   * @param {PaymentCreditoDebitoService} paymentService
   * @param {ValidatorService} validatorService
   * @param {PaymentDepositoService} paymentDepositoService
   * @param {PaymentPagoTiendaService} paymentServiceTD
   * @param {PaymentPaypalService} paymentPaypalService
   * @param {PaymentPayuService} paymentPayuService
   * @param {PaymentTarjetaDepartamentalService} paymentDepartamental
   * @param {PaymentTelmexService} paymentTelmexService
   * @param {PaymentTransferService} paymentTransferService
   * @param {ErrorService} errorService
   * @param {ConfigService} configService
   * @param {SucursalesService} sucursalesService
   * @param {GlobalService} globalService
   * @param {PaymentMonederoService} paymentMonederoService
   * @param {T1TarjetasService} t1TarjetaService
   * @param {PaymentT1PagosService} paymentT1Pagos
   * @param {TarjetaPagosService} tarjetaPagosService
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private multipedidoService: MultipedidoService,
    private tarjetasService: TarjetasCreditoDebitoService,
    private direccionesService: DireccionesService,
    private deviceService: DeviceFingerPrintService,
    private paymentService: PaymentCreditoDebitoService,
    private validatorService: ValidatorService,
    private paymentDepositoService: PaymentDepositoService,
    private paymentServiceTD: PaymentPagoTiendaService,
    private paymentPaypalService: PaymentPaypalService,
    private paymentPayuService: PaymentPayuService,
    private paymentDepartamental : PaymentTarjetaDepartamentalService,
    private paymentTelmexService : PaymentTelmexService,
    private paymentTransferService : PaymentTransferService,
    private errorService : ErrorService,
    private configService: ConfigService,
    private sucursalesService: SucursalesService,
    private globalService : GlobalService,
    private paymentMonederoService: PaymentMonederoService,
    private t1TarjetaService : T1TarjetasService,
    private paymentT1Pagos : PaymentT1PagosService,
    private tarjetaPagosService: TarjetaPagosService
  ) {
    this.config      = configService.config;
    this.idFpT1pagos = this.config.t1pagosId;
    this.idFpT1Amex  = this.config.t1pagosAmex;
    let secure = route.snapshot.params.secure != undefined ? route.snapshot.params.secure : undefined;
    if( secure != undefined ){
      if(secure == configService.config.secureKey){
        router.navigate(['/gracias-por-tu-confianza'])
      }else{
        this.isSetError = true
        this.globalService.setTdcError(true);
        errorService.setError('No se ha podido procesar su pago, intente con otro método')
        router.navigate(['/formas-pago'])
      }
    }
  }

  ngOnInit() {
    this.getMultipedido();
    this.checkout.deviceFinger = this.deviceService.cybs_dfprofiler('decompras', 'live');
    this.arriba()
    this.globalService.getCvv()
    this.codeSubscription = this.globalService.cvv
      .subscribe( cvv => {
        if( (/(^[0-9]+)$/.test(cvv)) ){
          this.showCode = false
        }
      });
  }

  ngOnDestroy(){
    //if( !this.isSetError ){
    //  this.errorService.setError('')
    //}
  }
  /**
   * Obtiene el objeto multipedido
   */
  private getMultipedido():void {

    this.multipedidoService.getMultipedido()
      .subscribe(multipedido => {
          this.multipedido = multipedido
          this.validateConditions();
          this.nombre      = multipedido.nombre
          this.facturacion = multipedido.facturacion
          this.mensualidad = multipedido.mensualidades
          this.formaPago   = multipedido.idFormaDePago
          this.id_forma    = multipedido.idFormaDePago;
          this.idFormaPago = multipedido.idFormaDePago;
          this.clickRecoge = multipedido.clickRecoge
          this.getSucursal(multipedido.sucursal)
          this.getQueryParams()
          this.getDireccion(multipedido.idDireccion)
          this.getTarjeta(multipedido.formaDePago);
          this.nombrePromo = this.multipedido.nombrePromocion;
          this.infoCargada = true;
          this.validateDisabled()
          this.captcha.attempts = multipedido.attempts
          this.isLoad = true
          this.montoPago = this.multipedido.montoPago;
      });
  }

  /**
   * Obtiene el objeto sucursal por medio del id
   */
  private getSucursal(id: number): void{
    if( isNumber(id) && id > 0){
      this.sucursalesService.getSucursal(id)
        .subscribe(( sucursal: Sucursal ) => {
          this.sucursal = sucursal;
          this.isLoadSucursal = true
        });
    }
  }
  /**
   * Obtiene el objeto direccion por medio del id
   */
  private getDireccion(id: number): void{
    if( isNumber(id) && id > 0){
      this.direccionesService.getDireccion(id)
        .subscribe((direccion: Direccion) => {
          this.nombre = direccion.nombre
          this.direccion = direccion;
          this.isLoadDireccion = true
        });
    }
  }

  /**
   * Obtiene el objeto tarjeta por medio del id
   */
  private getTarjeta(id: number): void{
    if( isNumber(id) && id > 0 && (
        this.formaPago == fpAmex ||
        this.formaPago == fpCreditoDebito ||
        this.formaPago == fpTarjetasDepartamentales ||
        this.formaPago === 4379 ||
        this.formaPago === fpCreditoRevolvente ||
        this.formaPago === fpMejorPlan ||
        this.formaPago === this.idFpT1pagos ||
        this.formaPago === this.idFpT1Amex
        )
    ){

      if(this.formaPago === this.idFpT1pagos || this.formaPago === this.idFpT1Amex ){
        this.tarjetaPagosService.getTarjeta(id)
          .subscribe((tarjeta: Tarjeta) =>{
            this.tarjeta = tarjeta
            this.isLoadTarjeta = true
            this.cvvHelp = tarjeta.tipo == 'amex' ? 'En caso de ser amex se muestra en la parte frontal de la tarjeta' : 'El cvv se encuentra en la parte posterior de la tarjeta';
            this.t1TarjetaService.getTarjeta(tarjeta.token).subscribe(
              response => {
                let direccion = response.data.tarjeta.direccion;
                this.setObjectDireccion(direccion);
                this.direccionesService.setDireccionFacturacion(this.facturacion).subscribe();
              }
            );
          });
      } else {

        if(!this.multipedido.facturacion.direccion){
          this.facturacion = this.direccion;
        }
        this.tarjetasService.getTarjeta(id)
          .subscribe((tarjeta: Tarjeta) => {
            this.tarjeta = tarjeta
            this.isLoadTarjeta = true
            if(this.tarjeta.tipo === 'sears' || this.tarjeta.tipo === 'sanborns'){
              this.showCode = false;
            }
            this.cvvHelp = tarjeta.tipo == 'amex' ? 'En caso de ser amex se muestra en la parte frontal de la tarjeta' : 'El cvv se encuentra en la parte posterior de la tarjeta';
          });
      }
    }
  }

  setObjectDireccion(direccion : T1Direccion):void {
    this.facturacion                = new Direccion();
    this.facturacion.direccion      = direccion.linea1;
    let arrLinea2                   = direccion.linea2.split(',');
    this.facturacion.calle          = direccion.linea1;
    this.facturacion.numeroExterior = arrLinea2[0].trim();
    this.facturacion.numeroInterior = arrLinea2[1].trim();
    this.facturacion.colonia        = arrLinea2[2].trim();
    this.facturacion.codigoPostal   = direccion.cp;
    this.facturacion.estado         = direccion.estado;
    this.facturacion.municipio      = direccion.municipio;
    this.facturacion.ciudad         = direccion.ciudad;
    if(direccion.hasOwnProperty('telefono')) {
      this.facturacion.telefono     = direccion.telefono.numero.toString();
    }
  }

  /**
   * Realiza la operacion de pago
   */
  public pagar(): void{

    let valid = this.validateCheckout(undefined);
    if(valid === true){
      switch(this.formaPago){
        case fpAmex :
        case fpCreditoDebito:
          this.paymentService.checkout(this.checkout)
          .subscribe((checkout: Checkout) => {
            this.captcha.attempts = checkout.attempts
            this.processCheckout(checkout)
          });
        break;
        case fpDepositoBancario:
          this.paymentDepositoService.checkout(this.checkout)
            .subscribe((checkout: Checkout) => {
              this.processCheckout(checkout)
            });
          break;
        case fpTiendasDepartamentales:
           this.paymentServiceTD.checkout(this.checkout)
               .subscribe((checkout: Checkout) => {
                 this.processCheckout(checkout)
               });
           break;
        case fpPaypal:
          this.paymentPaypalService.checkout(this.checkout)
          .subscribe((checkout: Checkout) => {
            this.processCheckout(checkout)
          });
          break;
        case fpTiendasConveniencia:
          this.paymentPayuService.checkout(this.checkout)
          .subscribe((checkout: Checkout) => {
            this.processCheckout(checkout)
          });
          break;
        case fpTarjetasDepartamentales:
        case fpMejorPlan:
        case fpCreditoRevolvente:
        case 4379:
          this.paymentDepartamental.checkout(this.checkout)
            .subscribe((checkout: Checkout) => {
              this.processCheckout(checkout)

            });
          break;
        case fpTelmex:
          this.paymentTelmexService.checkout(this.checkout)
          .subscribe((checkout: Checkout) => {
            this.processCheckout(checkout)

          });
          break;
        case 4448:
          this.paymentTransferService.checkout(this.checkout)
          .subscribe((checkout: Checkout) => {
            this.processCheckout(checkout)
          });
          break;
        case fpMonedero:
          this.paymentMonederoService.checkout(this.checkout)
            .subscribe((checkout: Checkout) => {
              this.processCheckout(checkout)
            });
          break;
        case this.idFpT1pagos:
        case this.idFpT1Amex:
          let tarjeta = new T1TarjetaUpdate();
          tarjeta.cvv2 = this.checkout.codigo;
          this.t1TarjetaService.updateTarjeta(this.tarjeta.token, tarjeta).subscribe(( response) => {
            delete this.checkout.codigo;
            this.paymentT1Pagos.checkout(this.checkout)
              .subscribe((checkout: Checkout) => {
                this.processCheckout(checkout)
              });
          });
        break;
      }//end switch
    } else {
      this.detallePago.focusCvv()

    }
  }

  /**
   * Valida los parametros que se envian en request pagar
   */
  public validateCheckout(event:any): boolean{
    let valid: boolean = true;
    switch(this.formaPago){
      case fpAmex :
      case fpCreditoDebito:
      case this.idFpT1pagos:
      case this.idFpT1Amex:
        this.rules = {
          'codigo' : new Validator(
            'codigo',
            String(this.checkout.codigo),
            'cvv',
            'El campo codigo es requerido'
          ),
          'captcha' : new Validator(
            'captcha',
            '',
            'captcha',
            'El campo captcha es requerido',
            false,
            this.captcha
          )
        }
      break;

    }//end switch
    let response = this.validatorService.validate(this.rules);
    for (let key in response){
      if (response.hasOwnProperty(key)){
        if ( response[key].error ){
          valid = false;
        }//end if
      }//end if
    }//end for
    this.disabled = !valid
    this.rules = response;

    return valid
  }

  /**
   * Genera el proceso de la forma de pago
   * @param checkout
   */
  public processCheckout(checkout: Checkout): void{
    if(checkout.status === true){
      console.log('checkout true');
      switch(this.formaPago){
        case fpAmex :
        case fpCreditoDebito:
        case fpDepositoBancario:
        case fpTiendasDepartamentales:
        case fpPaypal:
        case fpTiendasConveniencia:
        case fpTelmex:
        case this.idFpT1pagos:
        case this.idFpT1Amex:
          this.goThanckYouPage()
          this.globalService.deleteCvv()
          break;
        case fpTarjetasDepartamentales:
        case fpMejorPlan:
        case fpCreditoRevolvente:
        case 4379:
        case 4448:
        case fpMonedero:
          this.goThanckYouPage()
          break;
      }//end switch
    }else{
      // Seteo el error que envía el checkout
      this.errorService.setError(checkout.message);
      switch(this.formaPago){
        case fpAmex : // Tarjetas Debito Credito
        case fpCreditoDebito:
        case this.idFpT1pagos:
        case this.idFpT1Amex:
          this.error = checkout.message;
          if(checkout.attempts >= 3){
            let message = 'Se ha cerrado tu sesión.'
            this.errorService.setError(message)
            this.globalService.setError(true)
            this.globalService.setMessage(message)
            window.location.href = this.configService.getConfig().baseUrl + 'login/login/logout'
            return
          }//end if
          switch(checkout.action) {
            case 'editar':
              console.log(this.formaPago);
              if (this.formaPago == this.idFpT1pagos || this.formaPago == this.idFpT1Amex){
                this.router.navigate(['/formas-pago/t1pagos/'+ this.tarjeta.token]);
              }else {
                this.router.navigate(['/formas-pago/tarjetas/'+ this.tarjeta.id]);
              }
              break;
            case 'captcha':
            case 'formas-pago':
              this.globalService.setTdcError(true);
              this.router.navigate(['/formas-pago']);
              break;
            case 'home':
              window.location.href = this.configService.getConfig().baseUrl + 'login/login/logout'
              break;
            case '3d':
              if(this.formaPago == this.idFpT1pagos || this.formaPago == this.idFpT1Amex){
                this.secureRequestT1(checkout);
              } else {
                this.secureRequest(checkout);
              }
              break;
          }
          this.arriba()
          break;
        case fpMonedero:
        case fpDepositoBancario: // Deposito Bancario
          this.error = checkout.message
          this.arriba()
          this.router.navigate(['/formas-pago']);
          break;
        case fpTiendasDepartamentales: // Tiendas Departamentales / Conveniencia
          this.error = checkout.message
          this.arriba()
          this.router.navigate(['/formas-pago']);
           break;
        case fpPaypal: // Paypal
          this.error = checkout.message
          this.processCheckoutPaypal(checkout)
          this.arriba()
          break;
        case fpTiendasConveniencia: // Oxxo y Seven
          this.error = checkout.message
          this.arriba()
          this.router.navigate(['/formas-pago']);
          break;
        case fpTarjetasDepartamentales:
        case fpMejorPlan:
        case fpCreditoRevolvente:
        case 4379:
          this.error = checkout.message;
          this.arriba()
          this.globalService.setTdcError(true);
          this.router.navigate(['/formas-pago']);
          break;
        case fpTelmex:
          this.error = checkout.message
          this.arriba()
          break;
        case 4448:
          this.error = checkout.message
          this.arriba()
          this.router.navigate(['/formas-pago']);
          break;
      }//end switch
    }//end if
  }

  /**
   * Redirecciona a la pagina thanck you page
   */
  goThanckYouPage(): void {
    this.router.navigate(['/gracias-por-tu-confianza']);
  }

  /**
   *
   */
  public arriba(): void{
    window.scrollTo(0,0);
  }

  /**
   * Obtiene los parametros get paypal
   */
  private getQueryParams(): void{

    this.route.queryParams
    .subscribe((params) => {
      if (params.hasOwnProperty('token')){
        this.checkout.token = params.token;
      }//end if
      if (params.hasOwnProperty('PayerID')){
        this.checkout.payerID = params.PayerID;
      }//end if
      if (this.checkout.payerID !== undefined && this.checkout.token !== undefined) {
        this.pagar()
      }
    });
  }

  private processCheckoutPaypal(checkout: Checkout ): void{
    if(checkout.status == false && checkout.url !== undefined ){
      window.location.href = checkout.url
    }else{
      this.errorService.setError('Lo sentimos, algo salió mal. Elige otra forma de pago');
      this.router.navigate(['/formas-pago']);
    }//end if
  }

  /**
   *
   */
  //private actionCaptcha(){
  //  if((this.formaPago == fpAmex  || this.formaPago == fpCreditoDebito)){
  //    this.captcha.attempts = this.multipedido.attempts
  //    this.detallePago.actionCaptcha();
  //  }
  //}

  /**
   * validate conditions
   */
  private validateConditions(): void{

    if( this.multipedido.clickRecoge ){
      return;
    }else{
      switch(this.multipedido.idFormaDePago){
        case fpPaypal:
          this.conditions = ['idCliente', 'idFormaDePago', 'idDireccion'];
          break;
        default:
          break;
      }//end switch
    }//end if
    for (let key of this.conditions){
      if ( typeof this.multipedido[key] == 'undefined' || !this.multipedido[key]){
        this.router.navigate(['/direcciones']);
      }//end if
    }//end if
  }

  /**
   *
   */
  private validateDisabled(): void{
    switch(this.formaPago){
      case fpAmex :
      case fpCreditoDebito:
      case this.idFpT1pagos:
      case this.idFpT1Amex:
        if(this.showCode){
          this.disabled = true
        }else{
          this.disabled = false
        }//end if
        break;
      case fpDepositoBancario:
      case fpTiendasDepartamentales:
      case fpPaypal:
      case fpTiendasConveniencia:
      case fpTarjetasDepartamentales:
      case fpMejorPlan:
      case fpCreditoRevolvente:
      case 4379:
      case fpTelmex:
      case 4448:
      case fpMonedero:
        this.disabled = false
        break;
    }
  }

  /**
   * Secure request
   * @param response
   */
  private secureRequest(response : Checkout) {
    let params : any    = response.data
    let method : string = 'post'
    let form = document.createElement('form')
    form.setAttribute('method', method)
    form.setAttribute('action', this.config.secureEndPoint)
    for (let key in params){
      if( params.hasOwnProperty(key) ){
        let hiddenField = document.createElement('input')
        hiddenField.setAttribute('type', 'hidden')
        hiddenField.setAttribute('name', key)
        hiddenField.setAttribute('value', params[key])
        form.appendChild(hiddenField)
      }//end if
    }//end for
    document.body.appendChild(form)
    form.submit()
  }

  private secureRequestT1(response : Checkout) {
    let params : any    = response.data;
    let method : string = 'get';
    let form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', params.redirect);
    document.body.appendChild(form);
    form.submit();
  }

}
