export class Monedero {
  status          : boolean;
  procesa         : boolean;
  saldo           : number;
  monto           : number;
  total           : number;
}
