import {T1Direccion} from "./T1Direccion";
import {T1Telefono} from "./t1Telefono";

export class T1TarjetaCreate {
  nombre            : string;
  pan               : number;
  cvv2              : number;
  expiracion_mes    : number;
  expiracion_anio   : number;
  cliente_id        : string;
  default           : boolean;
  cargo_unico       : boolean;
  direccion         : T1Direccion;
  telefono          : T1Telefono;
}
