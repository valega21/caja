export class T1Telefono {
  tipo        : string;
  codigo_pais : number;
  codigo_area : string;
  prefijo     : string;
  numero      : number;
  extension   : number;
}
