export class Tarjeta {
    id          : number;
    nombre      : string;
    digitos     : string;
    apellido    : string;
    mes         : string;
    anio        : string;
    flag        : boolean;
    tipo        : string;
    predet      : string;
    bm          : string;
    idFormaPago : number;
    token       : string;
}
