export class Direccion {
    id            : number;
    nombre        : string;
    pais          : string;
    estado        : string;
    ciudad        : string;
    colonia       : string;
    calle         : string;
    codigoPostal  : string;
    numeroExterior: string;
    numeroInterior: string;
    telefono      : string;
    entreCalles   : string;
    referencias   : string;
    municipio     : string;
    accesibilidad : string;
    direccion     : string;
    action        : number;
    codigoEstado  : string;
}
