export class T1Tarjeta {
  token             : string;
  pan               : string;
  terminacion       : number;
  nombre            : string;
  expiracion_mes    : number;
  expiracion_anio   : number;
  iin               : number;
  marca             : string;
  cliente_id        : string;
  default           : boolean;
  cargo_unico       : boolean;
  creacion          : string;
  actualizacion     : string;
}
