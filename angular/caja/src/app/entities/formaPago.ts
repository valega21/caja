export class FormaPago {
    idMenuFormasdePago  : number;
    idFormadePago       : number;
    formadePago         : string;
    logo                : string;        
    descripcion         : string;       
    typo                : number;
    status              : number;
    order               : number;
}