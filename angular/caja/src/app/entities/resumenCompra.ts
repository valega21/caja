export class ResumenCompra {
  costoEnvio     : number;
  descuentoCredito: number;
  descuentoCupon  : number;
  descuentoFpago  : number;
  grupos          : any;
  productos       : any;
  subtotal        : number;
  total           : number;

}
