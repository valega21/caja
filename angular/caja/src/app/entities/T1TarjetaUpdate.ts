import {T1Direccion} from "./T1Direccion";
import {T1Telefono} from "./t1Telefono";

export class T1TarjetaUpdate {
    nombre          : string;
    expiracion_mes  : number;
    expiracion_anio : number;
    cvv2            : number;
    cliente_id      : string;
    default         : boolean;
    direccion       : T1Direccion;
    telefono        : T1Telefono;
}
