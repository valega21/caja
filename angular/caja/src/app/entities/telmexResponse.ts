export class TelmexResponse {
    error       : string;
    message     : string;
    status      : boolean;
    registro    : string;
    certificado : boolean;
    tipo        : number;
    telefono    : number;
}