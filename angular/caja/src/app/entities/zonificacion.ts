export class Zonificacion {
    estado: string;
    municipio: string;
    ciudad: string;
    colonias: string[];
    estatus: boolean;
    error: string;
    codigoEstado: string
}
