import { Injectable } from '@angular/core';
import {Checkout} from "../entities/checkout";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {ConfigService} from "./config.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Config} from "../entities/config";
import {of} from "rxjs/observable/of";
import {Monedero} from "../entities/monedero";

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class PaymentMonederoService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   *
   * @param {HttpClient} http
   * @param {ConfigService} configService
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){
    this.config   = this.configService.config;
    this.endPoint = this.config.apiEndPoint + '/monedero'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Realiza el proceso de pago
   * @param checkout
   */
  checkout(checkout: Checkout): Observable<Checkout> {
    const url = `${this.endPoint}`;
    return this.http.post<Checkout>(url, checkout, httpSetOptions).pipe(
      catchError(this.handleError<Checkout>('checkout'))
    );
  }

  getMonedero(){
    const url = `${this.endPoint}`;
    return this.http.get<Monedero>(url, httpGetOptions).pipe(
      catchError(this.handleError<any>('getMonedero'))
    );
  }

  /**
   *  Obtiene la direccion por medio del id
   */
  getThankYouPageData(): Observable<any> {
    const id : number = Math.floor(Math.random()*(99999-999+1)+999);
    const url = `${this.endPoint}/${id}`
    return this.http.get<any>(url, httpGetOptions).pipe(
      catchError(this.handleError<any>(`getThankYouPageData id=${id}`))
    );
  }

}
