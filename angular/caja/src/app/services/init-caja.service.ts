import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';
import { InitResponse } from '../entities/initResponse';
import { GlobalService } from '../services/global.service';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class InitCajaService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string ;

  /**
   * Initialized
   */
  private initialized : boolean;

  public isApp : boolean = false;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService : ConfigService,
    private cookieService : CookieService,
    private globalService : GlobalService
  ){ 
    this.config = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/inicio-caja'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {

    return (error: any): Observable<T> => {
      this.globalService.isAppCS.subscribe( isApp => {
        this.isApp = isApp;
      });
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      if (!this.isApp) {
        window.location.href = this.config.baseUrl + 'carrito'
      }
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   *  Inicia la caja por medio del token
   */
  initCaja(): Observable<InitResponse> {
    //this.cookieService.set(this.config.cookieName, 'b9bef62105116b52b6ac3edbdb4b7a94')
    localStorage.removeItem(this.configService.config.codeName);
    localStorage.removeItem(this.configService.config.tokenName);
    localStorage.removeItem(this.configService.config.crName);
    let id = this.cookieService.get(this.config.cookieName)
    const url = `${this.endPoint}/${id}`;
    return this.http.get<InitResponse>(url, httpGetOptions).pipe(
      tap((response: InitResponse) => this.goCart(response)),
      catchError(this.handleError<InitResponse>(`initCaja id=${id}`))
    );
  }

  /**
   * 
   * @param response 
   */
  private goCart(response: InitResponse): void{
    this.globalService.isAppCS.subscribe( isApp => {
      this.isApp = isApp;
    });
    if(response.error === true){
      switch(response.action){
        case 2:
          if (!this.isApp) {
            window.location.href = this.config.baseUrl + 'login'
          }
          break;
        case 3:
          if (!this.isApp) {
            window.location.href = this.config.baseUrl + 'carrito'
          }
          break;
      }
    }else{
      this.globalService.setInitialized(true)
    }
  }

}
