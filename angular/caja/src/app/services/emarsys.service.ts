import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { of } from 'rxjs/observable/of';

import { Emarsys } from '../entities/emarsys'

var proxyImages : string | null = null
var productosEmarsys : Emarsys[] = <Array<Emarsys>>[]

@Injectable()

export class EmarsysService {

  /**
   * Constructor
   */
  constructor() { }

  /*
  * Funcion encargada de generar las recomendaciones
  * en el thank you page de las formas de pago.
  */
  public getRecomendados(datos: any): Observable<Emarsys[]> {
    window['ScarabQueue'] = window['ScarabQueue'] || [];
    if(typeof window['ScarabQueue'].push === 'function') {
      proxyImages = datos.proxyImages;
      if (datos['testMode']) {
        window['ScarabQueue'].push(datos.testMode);
      }//end if
      window['ScarabQueue'].push(['setEmail', datos.correoCliente])
      window['ScarabQueue'].push(['cart', datos.productos])
      window['ScarabQueue'].push(['purchase', {
        orderId: datos.numeroPedido,
        items: datos.productos
      }])
      window['ScarabQueue'].push(['recommend', {
        logic: 'CART',
        containerId: datos.idContenedor,
        //templateId  : 'RelacionadoNav_nuevo',
        limit: datos.numeroItems,
        success: this.tratamientoImagenes
      }])
      window['ScarabQueue'].push(['go'])
    }

    return of(productosEmarsys)
  }

  /**
   * Tratamiento de imagenes
   * @param SC
   * @param render
   */
  public tratamientoImagenes(SC, render) {
    for (let product of SC.page.products) {
      product.title = product.title
      product.precio = product.price
      product.msrp_sub = product.msrp
      product.categoria2 = product.categoria1 = ""
      product.mensualidades = ""
      if (product.c_mensualidades != null && product.c_mensualidades != "") {
        product.mensualidades = product.c_mensualidades
      }
      if (product.category != null) {
        product.categoria1 = product.category
        product.categoria2 = product.category.split(">")[0]
        if (proxyImages) {
          product.imagen = product.image.replace("http://imagenes.sanborns.com.mx/", proxyImages + "/imagenes-sanborns-ii/")
          product.imagen = product.imagen.replace("https://imagenes.sanborns.com.mx/", proxyImages + "/imagenes-sanborns-ii/")
          product.imagen = product.imagen.replace("http://medios.plazavip.com/", proxyImages + "/medios-plazavip/")
          product.imagen = product.imagen.replace("https://medios.plazavip.com/", proxyImages + "/medios-plazavip/")
          product.imagen = product.imagen.replace("https://medios-plazavip.sears.com.mx/", proxyImages + "/medios-plazavip/")
          product.imagen = product.imagen.replace("http://medios-plazavip.sears.com.mx/", proxyImages + "/medios-plazavip/")
          product.imagen = product.imagen.replace("http://imagenes.claroshop.com/", proxyImages + "/imagenes-claroshop/")
          product.imagen = product.imagen.replace("https://imagenes.claroshop.com/", proxyImages + "/imagenes-claroshop/")
          product.imagen = product.imagen.replace("https://tienda.telmex.com", proxyImages + "/tienda-telmex/")
          product.imagen = product.imagen.replace("http://img.plazavip.com/", proxyImages + "/img-plazavip/")
          product.imagen = product.imagen.replace("https://img.plazavip.com/", proxyImages + "/img-plazavip/")
          product.imagen = product.imagen.replace("http://imgs.plazavip.com/", proxyImages + "/imgs-plazavip/")
          product.imagen = product.imagen.replace("https://imgs.plazavip.com/", proxyImages + "/imgs-plazavip/")
        }
      }
      let producto : Emarsys = {} as Emarsys ;
      producto.id     = product.id
      producto.title  = product.title
      producto.imagen = product.imagen
      producto.precio = product.precio;
      producto.c_descuento = product.c_descuento
      producto.precioLista = product.msrp_sub
      productosEmarsys.push(producto)
    }
    //return render(SC)
  }

  /**
   * Normalizar caracter
   * @param str
   */
  private normalizarCaracter(str: string):string {
    let from    = "ÃÀÄÂÈËÊÌÏÎÒÖÔÙÜÛãàäâèëêìïîòöôùüûÑñÇçÃ³"
    let to      = "aaaaeeeiiiooouuuaaaaeeeiiiooouuunnccó"
    let mapping = {}
    let ret     = []
    for(var i = 0, j = from.length; i < j; i++ ){
      mapping[ from.charAt( i ) ] = to.charAt( i )
    }
    for( var i = 0, j = str.length; i < j; i++ ) {
        var c = str.charAt( i )
        if( mapping.hasOwnProperty( str.charAt( i ) ) ){
          ret.push( mapping[ c ] );
        }else{
          ret.push( c );
        }
    }
    return ret.join( '' );
  }

  /**
   * Normalizar
   * @param str
   */
  private normalizar(str: string):string {
    let from    : string = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç/ "
    let to      : string = "aaaaaeeeeiiiioooouuuuaaaaaeeeeiiiioooouuuunncc-- "
    let mapping : any    = {}
    let ret     : string[] = []
    for(var i = 0, j = from.length; i < j; i++ ){
      mapping[ from.charAt( i ) ] = to.charAt( i )
    }
    for( var i = 0, j = str.length; i < j; i++ ) {
        var c = str.charAt( i )
        if( mapping.hasOwnProperty( str.charAt( i ) ) ){
          ret.push( mapping[ c ] );
        }else{
          ret.push( c );
        }
    }
    return ret.join( '' );
  }

}
