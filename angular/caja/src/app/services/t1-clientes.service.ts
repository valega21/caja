import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {ConfigService} from "./config.service";
import {of} from "rxjs/observable/of";
import {T1Response} from "../entities/t1Response";
import {Config} from "../entities/config";
import {T1TarjetaCreate} from "../entities/T1TarjetaCreate";

const httpSetOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const httpGetOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
};

@Injectable()
export class T1ClientesService {

  /**
   * Config object
   */
  private config: Config = new Config();

  /**
   * End point rest service
   */
  private endPoint: string;
  /**
   * Token t1
   */
  private token : string;

  /**
   *
   * @param {HttpClient} http
   * @param {ConfigService} configService
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) {
    this.config = this.configService.config;
    this.endPoint = this.config.apiEndPoint + '/cliente';
    //this.endPoint = 'https://api.caja.local.com/caja/api/mocksCliente';
    this.token = this.config.t1pagosToken;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Obtiene el id del cliente t1 pagos
   * @returns {Observable<any>}
   */
  getCliente(): Observable<any> {
    const url = `${this.endPoint}`;
    return this.http.get<any>(url, httpGetOptions).pipe(
      catchError(this.handleError<any>(`getCliente`))
    );
  }

  /**
   * Crea el cliente y regresa el id de t1 pagos
   * si ya existe el cliente, sólo regresa el id de t1 pagos
   * @returns {Observable<any>}
   */
  addCliente(): Observable<any> {
    const url = `${this.endPoint}`;
    return this.http.post<any>(url, {}, httpSetOptions).pipe(
      catchError(this.handleError<any>('addTarjeta'))
    );
  }
}
