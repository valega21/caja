import { Injectable } from '@angular/core';

@Injectable()
export class DatalayerService {

  constructor() {

  }

  /**
   * Step 2 del proceso de Checkout,
   * Se agrega este paso cuando se elige la dirección
   */
  public setCheckoutStep2() {
    window['dataLayer'] = window['dataLayer'] || [];
    if(window['dataLayer'] !== undefined && typeof window['dataLayer'].push === 'function'){
      window['dataLayer'].push({
        event     : 'checkout',
        ecommerce : {
          checkout: {
            actionField: {
              step  : 2,
              option: 'Web'
            }
          }
        }
      });
    }
  }

  /**
   *
   * Step 3 del proceso de Checkout,
   * Se agrega este paso cuando se elige cualquier forma de pago
   *
   * @param formaPago
   */
  public setCheckoutStep3(formaPago) {
    window['dataLayer'] = window['dataLayer'] || [];
    if(window['dataLayer'] !== undefined && typeof window['dataLayer'].push === 'function') {
      window['dataLayer'].push({
        formaPago: formaPago,
        event: 'checkout',
        ecommerce: {
          checkout: {
            actionField: {
              step: 3,
              option: 'Web'
            }
          }
        }
      });
    }
  }

  /**
   * Step 4 del proceso de Checkout,
   * Se agrega este paso cuando se eligen las formas de pago:
   * - Recibo Telmex: En la seleccion de las mensualidades
   * - Tarjetas crédito, débito: En el cvv
   * - Tarjetas departamentales: En la seleccion de mensualidades
   */
  public setCheckoutStep4() {
    window['dataLayer'] = window['dataLayer'] || [];
    if(window['dataLayer'] !== undefined && typeof window['dataLayer'].push === 'function') {
      window['dataLayer'].push({
        event: 'checkout',
        ecommerce: {
          checkout: {
            actionField: {
              step: 4,
              option: 'Web'
            }
          }
        }
      });
    }
  }

  /**
   * thankyou page
   * @param total
   * @param email
   * @param numPedido
   * @param productosDatayer
   * @param productosPixel
   * @param productosAdwords
   * @param userId
   */
  public setPurchase (total, email, numPedido, productosDatayer, productosPixel, productosAdwords, userId) {
    window['dataLayer'] = window['dataLayer'] || [];
    if(window['dataLayer'] !== undefined && typeof window['dataLayer'].push === 'function') {
      window['dataLayer'].push({
        idProducto: productosDatayer,
        totalCarrito: total,
        ecomm_pagetype: 'purchase',
        ecomm_prodid: productosAdwords,
        ecomm_totalvalue: total,
        PageType: 'TransactionPage',
        emailAjax: email,
        userId: userId,
        ProductTransactionProducts: productosPixel,
        TransactionID: numPedido,
        event: 'checkout',
        ecommerce: {
          checkout: {
            actionField: {
              option: 'Web'
            }
          }
        }
      });
    }
  };

  /**
   * Pixel para enviar un producto a Facebook
   * @param nombre
   * @param categoria
   * @param ids
   * @param totalVenta
   * @param currency
   * @param pedidoNormalizado
   * @param stage
   * @param origen
   */
  public setFbPurchase(nombre, categoria, ids, totalVenta, currency, pedidoNormalizado, stage, origen){
    window['fbq'] = window['fbq'] || [];
    if(window['fbq'] !== undefined && typeof window['fbq'] === 'function') {
      window['fbq']('track', 'Purchase', {
        content_name: nombre,
        content_category: categoria,
        content_ids: ids,
        content_type: 'product',
        value: totalVenta,
        currency: currency,
        order_id: pedidoNormalizado,
        staging: stage,
        origen: origen
      });
    }
  }

  /**
   * Pixel para enviar 2 o más productos a Facebook
   * @param ids
   * @param totalVenta
   * @param currency
   * @param pedidoNormalizado
   * @param stage
   * @param origen
   */
  public setFbPurchases(ids, totalVenta, currency, pedidoNormalizado, stage, origen){
    window['fbq'] = window['fbq'] || [];
    if(window['fbq'] !== undefined && typeof window['fbq'] === 'function') {
      window['fbq']('track', 'Purchase', {
        content_ids: ids,
        content_type: 'product',
        value: totalVenta,
        currency: currency,
        order_id: pedidoNormalizado,
        staging: stage,
        origen: origen
      });
    }
  }

  public convertionAdwords(datos) {
    window['dataLayer'] = window['dataLayer'] || [];
    if (window['dataLayer'] !== undefined && typeof window['dataLayer'].push === 'function') {
      window['dataLayer'].push({
        'ecommerce': {
          'purchase': {
            'actionField': {
              'id': datos['pedidoNormalizado'],
              'revenue': datos['totalVenta'],
              'shipping': datos['costoEnvio'],
              'dimension2 ': datos['idCliente'],
              'dimension4 ': datos['direccion']['estado'],
              'dimension5 ': datos['direccion']['ciudad'],
              'dimension10 ': datos['direccion']['cp'],
              'dimension11 ': datos['formaPago']['nombre'],
              'dimension16 ': datos['ambiente'],
              'dimension17 ': datos['origen'],
              'dimension18 ': datos['tiempoEntrega']
            },
            'products': datos['productos']
          }
        },
        event: 'purchaseCaja'
      });
    }
  }

}
