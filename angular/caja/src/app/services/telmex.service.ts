import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';
import { DireccionTelmex } from '../entities/direccionTelmex';
import { TelmexResponse }  from '../entities/telmexResponse';
import { ConfigService } from '../services/config.service';
import { Config } from '../entities/config';

const httpSetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }) 
};

const httpGetOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()

export class TelmexService {

  /**
   * Config object
   */
  private config : Config = new Config();

  /**
   * End point rest service
   */
  private endPoint : string;

  /**
   * Constructor service
   * @param http
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ){ 
    this.config   = this.configService.config
    this.endPoint = this.config.apiEndPoint + '/telmex'
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Valida el tipo de linea telmex
   * @param data 
   */
  public validaTipoLinea(data : DireccionTelmex): Observable<TelmexResponse>{
    const id = data.telefono
    const url = `${this.endPoint}/${id}`;
    return this.http.get<TelmexResponse>(url, httpGetOptions).pipe(
      catchError(this.handleError<TelmexResponse>(`ValidaTipoLinea telefono=${id}`))
    );
  }
  
  /**
   * Login telmex
   * @param data DireccionTelmex
   */
  public loginTelmex(data : DireccionTelmex): Observable<any> {
    const url   = this.endPoint;
    data.action = 1
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('loginTelmex'))
    );
  }

  /**
   * Login Form telmex
   * @param data DireccionTelmex
   */
  public loginFormTelmex(data : DireccionTelmex): Observable<any> {
    const url   = this.endPoint;
    data.action = 2
    return this.http.post<any>(url, data, httpSetOptions).pipe(
      catchError(this.handleError<any>('loginFormTelmex'))
    );
  }

}
