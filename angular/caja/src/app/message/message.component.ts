import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.sass']
})

export class MessageComponent implements OnInit {

  public message : string = '';
  public type    : number = 0;

  constructor(
    private global: GlobalService
  ) { }

  ngOnInit() {
    this.global.message.subscribe( message => {
      this.message = message;
    });
    this.global.typeMessage.subscribe( type => {
      this.type = type;
    });
  }

}
