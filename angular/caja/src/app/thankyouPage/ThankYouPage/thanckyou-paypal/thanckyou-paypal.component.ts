import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-thanckyou-paypal',
  templateUrl: './thanckyou-paypal.component.html',
  styleUrls: ['./thanckyou-paypal.component.sass']
})
export class ThanckyouPaypalComponent implements OnInit {

  @Input('data') data ;

  constructor() { }

  ngOnInit() {
  }

}
