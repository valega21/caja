import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-thanckyou-transfer',
  templateUrl: './thanckyou-transfer.component.html',
  styleUrls: ['./thanckyou-transfer.component.sass']
})
export class ThanckyouTransferComponent implements OnInit {

  @Input('data') data ;

  constructor() { }

  ngOnInit() {
  }

}
