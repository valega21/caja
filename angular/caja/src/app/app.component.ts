import {Component, Inject, OnInit} from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { GlobalService } from './services/global.service';
import {ConfigService} from "./services/config.service";
import {Config} from "./entities/config";
import { DOCUMENT } from '@angular/common';
import {isString} from "util";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent implements OnInit{

  /**
   * Loading
   */
  public action  : number  = 0;
  public loading : boolean = false;
  public error   : boolean = false;
  public message : string  = '';
  public type    : number  = 0;
  public isApp : boolean = false;
  public config : Config = new Config();

  /**
   *
   * @param doc
   * @param router
   * @param globalService
   * @param configService
   */
  constructor(
    @Inject(DOCUMENT) private doc: any,
    private router: Router,
    private globalService: GlobalService,
    private configService: ConfigService,
  ){

    this.config = this.configService.getConfig();
    router.events.subscribe(event => {
      if(event instanceof NavigationStart) {
        
      }else if(event instanceof NavigationEnd) {
        
      }
    });
  }

  /**
   * Hook init()
   */
  ngOnInit() {
    this.globalService.action.subscribe( action => { 
      this.action = action
    });
    this.globalService.error.subscribe( error => {
      this.error = error
    });
    this.globalService.loading.subscribe( loading => {
      this.loading = loading
    });
    this.globalService.message.subscribe( message => {
      this.message = message
    });
    this.globalService.typeMessage.subscribe( type => {
      this.type = type
    });
    this.globalService.isAppCS.subscribe( isApp => {
      this.isApp = isApp;
    });

    if(this.isValidPixelId(this.config.tagManagerId)){
      this.getTagManager();
      console.log('TagManager Initialized');
    }

    if(this.isValidPixelId(this.config.emarsysId)){
      this.getEmarsysPixel();
      console.log('Emarsys Initialized');
    }

    if(this.isValidPixelId(this.config.facebookId)){
      this.getFacebookPixel();
      console.log('FacebookPixel Initialized');
    }
  }

  public getTagManager()
  {
    const script = this.doc.createElement('script');
    const head = this.doc.getElementsByTagName('head')[0];
    const body = this.doc.getElementsByTagName('body')[0];

    script.type = 'text/javascript';
    script.innerHTML =  '(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push'
        + '({\'gtm.start\':new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)'
        + '[0],j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src='
        + '\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);})'
        + '(window,document,\'script\',\'dataLayer\',\'' + this.config.tagManagerId + '\');';

    // Agrego el script del tagmanager al head
    head.appendChild(script);

    let noscript = this.doc.createElement('noscript');
    let iframe = this.doc.createElement('iframe');
    iframe.setAttribute("src", "https://www.googletagmanager.com/ns.html?id=" + this.config.tagManagerId);
    iframe.style.width = "50";
    iframe.style.height = "50";
    iframe.style.display = 'none';
    iframe.style.visibility = "hidden";
    noscript.appendChild(iframe);

    // Agrego el noscript del tagmanager al body
    body.appendChild(noscript);
  }

  public getFacebookPixel()
  {
    const script = this.doc.createElement('script');
    const head = this.doc.getElementsByTagName('head')[0];
    script.type = 'text/javascript';
    script.innerHTML =  '!function(f,b,e,v,n,t,s){' +
        '      if(f.fbq)return;n=f.fbq=function(){' +
        '        n.callMethod?' +
        '            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;' +
        '      n.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;' +
        '      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)' +
        '    }(window, document,\'script\',\'//connect.facebook.net/en_US/fbevents.js\');' +
        '    fbq(\'init\', \'' + this.config.facebookId + '\');' +
        '    fbq(\'track\', \'PageView\');';

    // Agrego el script de fb al head
    head.appendChild(script);
  }

  public getEmarsysPixel()
  {
    const script = this.doc.createElement('script');
    const head = this.doc.getElementsByTagName('head')[0];
    script.type = 'text/javascript';
    script.innerHTML = 'var ScarabQueue = ScarabQueue || [];' +
        '    (function(subdomain, id) {' +
        '      if (document.getElementById(id)) return;' +
        '      var js = document.createElement(\'script\'); js.id = id;' +
        '      js.src = subdomain + \'.scarabresearch.com/js/' + this.config.emarsysId + '/scarab-v2.js\';' +
        '      var fs = document.getElementsByTagName(\'script\')[0];' +
        '      fs.parentNode.insertBefore(js, fs);' +
        '    })(\'https:\' == document.location.protocol ? \'https://recommender\' : \'http://cdn\', \'scarab-js-api\');';

    // Agrego el script de emarsys al head
    head.appendChild(script);
  }

  public isValidPixelId(id): Boolean
  {
    let valid = false;
    if(isString(id) && id.length > 0) {
      valid = true;
    }
    return valid;
  }

}
